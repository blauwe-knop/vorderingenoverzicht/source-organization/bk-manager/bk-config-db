# bk-config-db Helm chart

By default, this chart uses a cloud-native postgresql setup. To use an external database instead, 
simply set `pgCloudNativeEnabled: false`.

## Postgresql secret
The cloud-native postgresql setup creates a secret like the following:
```
Name:         bk-config-db-app
Namespace:    bk-app-manager-organization
Labels:       cnpg.io/cluster=bk-config-db
              cnpg.io/reload=true
Annotations:  cnpg.io/operatorVersion: 1.22.0

Type:  kubernetes.io/basic-auth

Data
====
dbname:    12 bytes
uri:       124 bytes
password:  64 bytes
pgpass:    112 bytes
port:      4 bytes
user:      12 bytes
username:  12 bytes
host:      15 bytes
jdbc-uri:  143 bytes
```
When disabling cloud-native postgresql, be sure to manually create a secret like above, 
because "username" and "password" are still being used.
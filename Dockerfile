FROM debian:bookworm-slim

ARG TARGETARCH
ARG USER_ID=10001
ARG GOLANG_MIGRATE_VERSION=v4.17.1

# Enable HTTPS for apt
RUN apt-get update && \
    apt-get install -y --no-install-recommends \
    apt-transport-https \
    ca-certificates \
    gnupg-agent

# Install psql
RUN apt-get update && \
    apt-get install -y --no-install-recommends curl ca-certificates gnupg && \
    curl -L https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add - && \
    echo "deb https://apt.postgresql.org/pub/repos/apt/ bookworm-pgdg main" > /etc/apt/sources.list.d/postgres.list && \
    apt update && \
    apt install -y --no-install-recommends postgresql-client-15 python3 && \
    rm -rf /var/lib/apt/lists/*

# Install migrate
RUN curl -k -O -L https://github.com/golang-migrate/migrate/releases/download/${GOLANG_MIGRATE_VERSION}/migrate.linux-${TARGETARCH}.deb && \
    apt-get install -y --no-install-recommends ./migrate.linux-${TARGETARCH}.deb

# Add non-priveleged user. Disabled for openshift
RUN useradd -u "$USER_ID" appuser

COPY --chown=appuser ./migrations /migrations
COPY --chown=appuser ./script/upgrade-db.sh  /usr/local/bin/upgrade-db.sh

USER appuser
HEALTHCHECK none
RUN ["chmod", "+x", "/usr/local/bin/upgrade-db.sh"]
